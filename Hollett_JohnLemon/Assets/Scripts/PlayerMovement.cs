﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    Vector3 m_Movement;
    Animator m_Animator;
    Quaternion m_Rotation = Quaternion.identity;
    Rigidbody m_RigidBody;

    AudioSource m_AudioSource;

    public float turnSpeed = 20f;

    // sets up health and invincible for player, allows reference to GameEnding script
    public int Health;
    bool invincible;
    public GameEnding gameEnding;

	// variables for HealthUI
	public Text healthUI;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_RigidBody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

		healthUI.text = "Health: " + Health; // sets up HealthUI at the beginning of game
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    private void OnAnimatorMove()
    {
        m_RigidBody.MovePosition(m_RigidBody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_RigidBody.MoveRotation(m_Rotation);
    }

    public void takeDamage(int damage) // setups public function for enemies to call to damage player
    {
        if (Health >= 0 && !invincible) // checks to see if Health is greater than 0 and that invincible is not active
        {
            Health -= damage; // subtracts enemy damage from player health
			healthUI.text = "Health: " + Health; // updates HealthUI everytime player takes damage

            invincible = true; // sets invincible to true so player doesn't take damage every frame
            Invoke("endInvicible", .5f); // calls endInvinvicible after two seconds so the player can take damage again

            if (Health <= 0) // checks to see if player is at or below 0 health, then ends the game
            {
                gameEnding.CaughtPlayer();
            }
        }
    }

    void endInvicible() // ends invincible
    {
        invincible = false;
    }
}
