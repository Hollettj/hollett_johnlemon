﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingEnemy : MonoBehaviour
{
    public Transform player;
    public PlayerMovement playerMovement;
    public GameEnding GameEnding;
    public WaypointPatrol waypointPatrol;
    bool m_IsPlayerInRange;

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update()
    {
        if (m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    waypointPatrol.aggro = true; // sets enemy to aggro against player
                    playerMovement.takeDamage(2); // damages the player
                }
                else
                {
                    waypointPatrol.aggro = false; // sets enemy aggro off if player not "seen" by player
                }
            }
        }
    }
}
