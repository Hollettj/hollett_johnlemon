﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lockpick : MonoBehaviour
{
    bool playerInArea;
    float timeKeyHeldDown = 0f; // how long the key has been held down
    public float lockpickTime = 4f; // how long it takes for the player to lockpick the door

	// variables for lockpick prompt
	public Text lockpickPrompt;

	// variables for animation
	Animation anim;
	bool DoorOpen = false;

	private void Start()
	{
		lockpickPrompt.text = ""; // defaults to no prompt active
		anim = GetComponent<Animation>();
	}

	void OnTriggerEnter (Collider other) // checks to see when the player enters the area by the door
    {
        if (other.gameObject.CompareTag("Player") && !DoorOpen)
        {
            playerInArea = true;
			lockpickPrompt.text = "Lockpick: Hold Space"; // makes prompt appear

            // print("Area Entered");
        }
    }

    void OnTriggerExit(Collider other) // checks to see when the player exits the area by the door
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerInArea = false;
			lockpickPrompt.text = ""; // gets rid of prompt

            // print("Area Exited");
        }
    }

    void Update()
    {
        if (playerInArea) // checks to see if player is in the area
        {
            if (Input.GetKey(KeyCode.Space)) // checks to see if player is holding the space key down
            {
                timeKeyHeldDown += Time.deltaTime; // adds the time passed to the amount of time the key has been held down
                // print("Holding Space");
                // print(timeKeyHeldDown);
                if (timeKeyHeldDown >= lockpickTime) // checks to see if the key has been held down long enough
                {
					lockpickPrompt.text = ""; // gets rid of prompt
					anim.Play();
					DoorOpen = true;
                }
            }
            else // if the player is not holding down the space key, resets the time for how long the key has been held down
            {
                timeKeyHeldDown = 0f;
            }
        }
    }
}
